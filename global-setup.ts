// global-setup.ts



const { chromium } = require('@playwright/test');

module.exports = async () => {
    const browser = await chromium.launch();
    const page = await browser.newPage();
    await page.goto('https://ylarod.ro/backend');
    await page.fill('[aria-label="Username or E-Mail"]', 'danielB');
    await page.fill('[aria-label="Password"]', '1990@dada');
    await page.click('button:has-text("Login")');
}