import { test, expect, Browser, BrowserContext, chromium, Page } from "@playwright/test"
// import {UsersAndPass} from "./usersAndPass.main";

export class LibraryUtils {

  // UsersAndPass: any  = new UsersAndPass();

  makeid(): string {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  makeCode(): string {
    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 4; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  makeSeries(): string {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

}