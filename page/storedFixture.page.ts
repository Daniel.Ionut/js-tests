import { test, expect, Browser, BrowserContext, chromium, Page } from "@playwright/test"


export class StoredFixture {
  textC: any;

  // UsersAndPass: any  = new UsersAndPass();

  storedCat1(cat1: string) {
      
     const textC = cat1;
    
    return textC;
    console.log(textC);
    
  }

  makeCode(): string {
    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 4; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  makeSeries(): string {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

}