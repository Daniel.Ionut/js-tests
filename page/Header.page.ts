import { Page } from "@playwright/test";

export default class HeaderPage{

    private page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    // Locators 

    public get eleConfiguration() {
        const ConfigurationBtn = this.page.$("text=Configuration");
        if(ConfigurationBtn != null) {
            return ConfigurationBtn;
        }else throw new Error("No Element");
    }

    public get eleVendors() {
        const VendorsBtn = this.page.$("text=Vendors");
        if(VendorsBtn != null) {
            return VendorsBtn;
        }else this.page.waitForLoadState;
    }

    public get eleProduse() {
        const ProduseBtn = this.page.$("text=Produse");
        if(ProduseBtn != null) {
            return ProduseBtn;
        }else this.page.waitForLoadState;
    }

    public get eleOferte() {
        const OferteBtn = this.page.$("text=Oferte");
        if(OferteBtn != null) {
            return OferteBtn;
        }else this.page.waitForLoadState;
    }

    public async clickConfiguration() {
        const ele = await this.eleConfiguration;
        await ele?.click();
    }

    public async clickVendors() {
        const ele = await this.eleVendors;
        await ele?.click();
    }

    public async clickProduse() {
        const ele = await this.eleProduse;
        await ele?.click();
    }

    public async clickOferte() {
        const ele = await this.eleOferte;
        await ele?.click();
    }


}