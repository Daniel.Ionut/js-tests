import { Page } from "@playwright/test";

export default class LoginBE {

    private page: Page;
    constructor(page: Page) {
        this.page = page;
    }

    public get eleEmailTextField() {
        return this.page.$("input[name='username']");
    }

    public get elePassTextField() {
        return this.page.$("input[name='password']");
    }
    
    public get eleLoginBtn() {
    return this.page.$("div[class='v-btn__content']");
    }
    

    public async enterUserName(name: string){
        const ele = await this.eleEmailTextField;
        await ele?.fill(name);
    }

    public async enterUserPassword(pass: string){
        const ele = await this.elePassTextField;
        await ele?.fill(pass);
    }

    public async clickLoginBtn(){
        const ele = await this.eleLoginBtn;
        await ele?.click();
    }

  
}