import { test, expect, chromium } from "@playwright/test";
import {Options} from './createCategories.test';
import HeaderPage from "../page/Header.page";
import LoginBE from "../page/LoginBE.page";
import { UsersAndPass } from "../page/usersAndPass.main";
import Env from "../utils/environment";
import { LibraryUtils } from "../utils/library.utils";

// import test, {expect} from "./cat.test";

test.describe('Create Vendors', () => {

    const userPass = new UsersAndPass();

     const cod = new LibraryUtils().makeCode();
    let id = new LibraryUtils().makeid();
    let serie = new LibraryUtils().makeSeries;
    let codeD = cod;
    let prefix = id;
    const emailDoraly = "doraly-" + prefix + "@automated.ro";
    const comapanieDoraly = "Companie Doraly " + prefix + " SRL";
    const serieFFDoraly = serie;
    let codeE = cod;
    const comapanieExtern = "Companie Extern " + prefix + " SRL";
    let serieFFExtern = serie;
    let serieFTExtern = serie;
    let newCompExtern = comapanieExtern;
    let newEmailExtern = userPass.email;
    const DPDuserExtern = "200900152";
    const DPDpassExtern = "8113918533";
    const FANuserExtern = "clienttest";
    const FANpassExtern = "testing";
    const FANclientId = "7032158";
    const SamedayUserExtern = "doralytest";
    const SamedayPassExtern = "/kGjaoLlTw==";
    const SamedayClientId = "22";


    test('create doraly vendor', async () => {

        const browser = await chromium.launch();
        const context = await browser.newContext();
        const page = await context.newPage();

        let header = new HeaderPage(page);
        let login = new LoginBE(page);

        await page.goto(Env.setBrowserBE);
        await expect(page.locator('.title')).toBeVisible();

        await login.enterUserName(userPass.usernameBE);
        await login.enterUserPassword(userPass.passBE);
        await login.clickLoginBtn();


        await page.locator('button:has-text("Vendors")').click();
        await page.locator('text=Comercianti').first().click();
        await expect(page).toHaveURL('https://ylarod.ro/backend#/admin/comercianti/vendors');
        // await header.clickVendors;

        await page.click("//span[contains(.,'ADAUGA COMERCIANT')]");

        //Tab Comercianti
        await page.fill("//input[@placeholder='Cod *']", codeD);
        await page.fill("//input[contains(@aria-label,'Email *')]", emailDoraly);
        await page.fill('[placeholder="Nume comercial \\*"]', comapanieDoraly);
        await page.fill('[placeholder="Nume companie \\*"]', comapanieDoraly);
        await page.fill('[placeholder="Adresa sediu comercial \\*"]', 'Acasa Test');
        await page.locator('text=Tip comerciant *arrow_drop_down >> div').first().click();
        await page.locator('.v-menu__content.theme--light.menuable__content__active .v-select-list .v-list div .v-list__tile .v-list__tile__content .v-list__tile__title').first().click();
        await page.fill('[placeholder="Administrator \\*"]', 'Administrator test');
        await page.fill('[placeholder="Telefon administrator \\*"]', '0766666666');
        await page.fill('[placeholder="Persoana de contact \\*"]', 'Contact test');
        await page.fill('[placeholder="Telefon persoana de contact \\*"]', '0744444444');
        await page.fill('[placeholder="IBAN \\*"]', '7894536123');
        await page.fill('[placeholder="Registrul comertului \\*"]', '4536987321');
        await page.fill('[placeholder="CUI \\*"]', '1324365987');
        
        // await page.locator('[aria-label="Responsabil Doraly"]').click();
        // await page.waitForSelector('text=Daniel Bacanu').click();

        //Tab Financiar
        await page.locator('a:has-text("Financiar")').click();
        await page.locator('[placeholder="Numar zile plata \\*"]').fill('15');
        await page.locator('.v-input.mx-2 .v-input__control .v-input__slot .v-input--selection-controls__input .v-input--selection-controls__ripple').first().click();
        await page.locator('.layout div:nth-child(4) .v-input .v-input__control .v-input__slot .v-input--selection-controls__input .v-input--selection-controls__ripple').click();
        await page.locator('.flex.xs2.md2.ml-5.v-if .v-input .v-input__control').click();
        await page.fill('[placeholder="Serie facturi fiscale \\*"]', new LibraryUtils().makeSeries());
        await page.locator('[placeholder="Start facturi fiscale \\*"]').fill('1');
        await page.locator('[placeholder="Stop facturi fiscale \\*"]').fill('100');

        await page.locator("(//i[contains(.,'edit')])[2]").click();
        await page.locator("(//div[@class='v-text-field__slot']//input)[1]").fill('0');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[3]").click();
        await page.locator("(//div[@class='v-text-field__slot']//input)[1]").fill('0');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[4]").click();
        await page.locator("(//div[@class='v-text-field__slot']//input)[1]").fill('0');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[5]").click();
        await page.locator("(//div[@class='v-text-field__slot']//input)[1]").fill('0');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[6]").click();
        await page.locator("(//input[@type='text'])[1]").fill('0');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[7]").click();
        await page.locator("(//input[@type='text'])[1]").fill('0');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[8]").click();
        await page.locator("(//input[@type='text'])[1]").fill('0');
        await page.locator("(//div[text()='Save'])[1]").click();

        //Tab Categorii
        await page.locator('#form-vendors a:has-text("Categorii")').click();
        await page.locator('button:has-text("add_circle Adauga comisioane pe categorie")').click();
        await page.click('[aria-label="Categorii"]');
        await page.fill('[aria-label="Categorii"]', "ae8yb");
        await page.focus('.v-menu__content.theme--light.menuable__content__active .v-select-list .v-list div .v-list__tile .v-list__tile__content');
        await page.click("(//div[contains(.,'Iluminat si Electrice aE8yb')])[7]");
        await page.locator('[aria-label="Nivel comision 1"]').fill('15');
        await page.locator('[aria-label="Nivel comision 2"]').fill('13');
        await page.locator('[aria-label="Nivel comision 3"]').fill('11');
        await page.locator('text=Propaga comisioane in subcategorii').click();
        await page.click("(//div[@class='v-btn__content'][contains(.,'Save')])[8]");
        await page.locator('button:has-text("save SALVEAZA")').click();


        const toastr = await page.$("//div[@class='toast-message']");
        if (toastr) {
            expect(await toastr.textContent()).toContain("Vendor created");
        }
        await page.waitForTimeout(1000);

        await page.reload;

        await page.close();


    })


    test('create extern vendor', async () => {

        const browser = await chromium.launch();
        const context = await browser.newContext();
        const page = await context.newPage();

        let header = new HeaderPage(page);
        let login = new LoginBE(page);

        await page.goto(Env.setBrowserBE);
        await expect(page.locator('.title')).toBeVisible();

        await login.enterUserName(userPass.usernameBE);
        await login.enterUserPassword(userPass.passBE);
        await login.clickLoginBtn();


        await page.locator('button:has-text("Vendors")').click();
        await page.locator('text=Comercianti').first().click();
        await expect(page).toHaveURL('https://ylarod.ro/backend#/admin/comercianti/vendors');
        // await header.clickVendors;

        await page.click("//span[contains(.,'ADAUGA COMERCIANT')]");

        //Tab Comercianti
        await page.fill("//input[@placeholder='Cod *']", codeE);
        await page.fill("//input[contains(@aria-label,'Email *')]", newEmailExtern);
        await page.fill('[placeholder="Nume comercial \\*"]', newCompExtern);
        await page.fill('[placeholder="Nume companie \\*"]', newCompExtern);
        await page.fill('[placeholder="Adresa sediu comercial \\*"]', 'Acasa Test');
        await page.locator('text=Tip comerciant *arrow_drop_down >> div').first().click();
        await page.locator('.v-menu__content.theme--light.menuable__content__active .v-select-list .v-list div:nth-child(2) .v-list__tile .v-list__tile__content .v-list__tile__title').click();        await page.fill('[placeholder="Administrator \\*"]', 'Administrator test');
        await page.fill('[placeholder="Telefon administrator \\*"]', '0766666666');
        await page.fill('[placeholder="Persoana de contact \\*"]', 'Contact test');
        await page.fill('[placeholder="Telefon persoana de contact \\*"]', '0744444444');
        await page.fill('[placeholder="IBAN \\*"]', '7894536123');
        await page.fill('[placeholder="Registrul comertului \\*"]', '4536987321');
        await page.fill('[placeholder="CUI \\*"]', '1324365987');

        //Tab Logistica
        await page.click("//a[contains(.,'Logistica')]");
        await page.fill("//input[@aria-label='Adresa']", "Str. Acasa, nr.12");
        await page.click("//input[contains(@aria-label,'Judet *')]");
        await page.fill("//input[contains(@aria-label,'Judet *')]", "BUCURESTI");
        await page.click("//span[contains(.,'BUCURESTI')]");
        await page.click("//input[contains(@aria-label,'Localitate *')]");
        await page.click("//a[@class='v-list__tile v-list__tile--link theme--light'][contains(.,'BUCURESTI')]");
        await page.fill("//input[contains(@aria-label,'Timp de livrare estimat')]", "3");
        await page.fill("//input[contains(@aria-label,'Intarziere maxima de livrare')]", "2");

        await page.locator('button:has-text("local_shipping Adauga logistica")').click();
        await page.locator('b:has-text("Alege curier")').click();
        await page.locator('text=Curier *Alege curierarrow_drop_down >> div').first().click();
        await page.locator('text=DPD').click();
        await page.fill("//input[contains(@aria-label,'Username')]", DPDuserExtern);
        await page.fill("//input[contains(@aria-label,'Password')]", DPDpassExtern);

        await page.locator('button:has-text("local_shipping Adauga logistica")').click();
        await page.locator('b:has-text("Alege curier")').click();
        await page.locator('text=Curier *Alege curierarrow_drop_down >> div').nth(2).click();
        await page.locator('text=Fan Courier').first().click();
        await page.fill("(//input[contains(@aria-label,'Username')])[2]", FANuserExtern);
        await page.fill("(//input[contains(@aria-label,'Password')])[2]", FANpassExtern);
        await page.fill("(//input[contains(@aria-label,'Client Id')])[2]", FANclientId);

        await page.locator('button:has-text("local_shipping Adauga logistica")').click();
        await page.locator('b:has-text("Alege curier")').click();
        await page.locator('text=Curier *Alege curierarrow_drop_down >> i').click();
        await page.locator('text=SameDay').first().click();
        await page.fill("(//input[contains(@aria-label,'Username')])[3]", SamedayUserExtern);
        await page.fill("(//input[contains(@aria-label,'Password')])[3]", SamedayPassExtern);
        await page.fill("(//input[contains(@aria-label,'Client Id')])[3]", SamedayClientId);
        await page.click("//label[contains(.,'Livreaza in lockere')]");

        
        // await page.locator('[aria-label="Responsabil Doraly"]').click();
        // await page.waitForSelector('text=Daniel Bacanu').click();

        //Tab Financiar
        await page.locator('a:has-text("Financiar")').click();
        await page.locator('[placeholder="Numar zile plata \\*"]').fill('15');
        await page.locator('.v-input.mx-2 .v-input__control .v-input__slot .v-input--selection-controls__input .v-input--selection-controls__ripple').first().click();
        await page.locator('.layout div:nth-child(4) .v-input .v-input__control .v-input__slot .v-input--selection-controls__input .v-input--selection-controls__ripple').click();
        await page.locator('.flex.xs2.md2.ml-5.v-if .v-input .v-input__control').click();
        await page.fill('[placeholder="Serie facturi fiscale \\*"]', new LibraryUtils().makeSeries());
        await page.locator('[placeholder="Start facturi fiscale \\*"]').fill('1');
        await page.locator('[placeholder="Stop facturi fiscale \\*"]').fill('100');

        await page.locator("(//i[contains(.,'edit')])[2]").click();
        await page.locator("(//div[@class='v-text-field__slot']//input)[1]").fill('1');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[3]").click();
        await page.locator("(//div[@class='v-text-field__slot']//input)[1]").fill('1');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[4]").click();
        await page.locator("(//div[@class='v-text-field__slot']//input)[1]").fill('1');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[5]").click();
        await page.locator("(//div[@class='v-text-field__slot']//input)[1]").fill('1');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[6]").click();
        await page.locator("(//input[@type='text'])[1]").fill('1');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[7]").click();
        await page.locator("(//input[@type='text'])[1]").fill('1');
        await page.locator("(//div[text()='Save'])[1]").click();

        await page.locator("(//i[contains(.,'edit')])[8]").click();
        await page.locator("(//input[@type='text'])[1]").fill('1');
        await page.locator("(//div[text()='Save'])[1]").click();

        //Tab Categorii
        await page.locator('#form-vendors a:has-text("Categorii")').click();
        await page.locator('button:has-text("add_circle Adauga comisioane pe categorie")').click();
        await page.click('[aria-label="Categorii"]');
        await page.fill('[aria-label="Categorii"]', "ae8yb");
        await page.focus('.v-menu__content.theme--light.menuable__content__active .v-select-list .v-list div .v-list__tile .v-list__tile__content');
        await page.click("(//div[contains(.,'Iluminat si Electrice aE8yb')])[7]");
        await page.locator('[aria-label="Nivel comision 1"]').fill('15');
        await page.locator('[aria-label="Nivel comision 2"]').fill('13');
        await page.locator('[aria-label="Nivel comision 3"]').fill('11');
        await page.locator('text=Propaga comisioane in subcategorii').click();
        await page.click("(//div[@class='v-btn__content'][contains(.,'Save')])[8]");
        await page.locator('button:has-text("save SALVEAZA")').click();


        const toastr = await page.$("//div[@class='toast-message']");
        if (toastr) {
            expect(await toastr.textContent()).toContain("Vendor created");
        }
        await page.waitForTimeout(1000);






    })

})