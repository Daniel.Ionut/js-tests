import { test, expect, chromium, Page, Browser, BrowserContext} from "@playwright/test";
import HeaderPage from "../page/Header.page";
import LoginBE from "../page/LoginBE.page";
import { UsersAndPass } from "../page/usersAndPass.main";
import Env from "../utils/environment";
import { LibraryUtils } from "../utils/library.utils";
import {StoredFixture} from "../page/storedFixture.page"

export type Options = { defaultItem: string };

test.describe('Categories', () => {
   
    let id = new LibraryUtils().makeid(); 
    const prefix = id;
    const cat1Name = 'Iluminat si electrice ' + prefix;
    const cat2Name = 'Iluminat ' + prefix;
    const cat3Name = 'Corpuri de iluminat interior ' + prefix;
    const cat4Name = 'Kit aplice ' + prefix;
   
    let header: HeaderPage;
    let login: LoginBE;
    let page: Page;
    let browser: Browser;
    let context: BrowserContext;
    let cat: StoredFixture;
    

    test('create categories', async () => {

        const userPass = new UsersAndPass();
         browser = await chromium.launch();
         context = await browser.newContext();
         page = await context.newPage();
        cat = new StoredFixture();
         header = new HeaderPage(page);
         login = new LoginBE(page);
        
        await page.goto(Env.setBrowserBE);
        await expect(page.locator('.title')).toBeVisible();

               await login.enterUserName(userPass.usernameBE);
               await login.enterUserPassword(userPass.passBE);
               await login.clickLoginBtn();
               await header.clickConfiguration;

               

        // await page.click('[aria-label="Username or E-Mail"]');
        // await page.fill('[aria-label="Username or E-Mail"]', userPass.usernameBE);
        // await page.click('[aria-label="Password"]');
        // await page.fill('[aria-label="Password"]', userPass.passBE);
        // await Promise.all([
        //     page.waitForNavigation(/*{ url: 'https://ylarod.ro/backend' }*/),
        //     page.click('button:has-text("Login")')
        // ]);

        await page.click('text=Configuration');
        // await expect(page).toHaveURL('https://www.ylarod.ro/backend#/admin/configuration');
        await page.locator('text=Gestionare Categoriikeyboard_arrow_down >> i').click();
        await page.locator('text=Categorii').nth(2).click();
        // await expect(page).toHaveURL('https://www.ylarod.ro/backend#/admin/configuration/lists/categories');

        await page.waitForSelector("(//div[@class='v-list__group'])");
        const ele = await page.$$("(//div[@class='v-list__group'])");
        //   console.log(ele.length);

        var newCode = ele.length + 1;
        //  console.log(newCode);
        var code: string = newCode.toString();

        // Lvl1 Category
        const newCat1 = cat1Name;
    //    cat = this.storedCat1(newCat1);
        
       
       
       
        await page.locator('button:has-text("add AddCategory")').click();
        await page.locator('[aria-label="Nume categorie \\*"]').click();
        await page.locator('[aria-label="Nume categorie \\*"]').fill(cat1Name);

        await page.locator('[aria-label="Cod \\*"]').fill(code);

        await page.locator('[aria-label="Prioritate \\*"]').fill(code);
        await page.locator('.v-input--selection-controls__ripple').first().click();
        await page.locator('[aria-label="Tva \\*"]').click();
        await page.locator('.v-menu__content.theme--light.menuable__content__active .v-select-list .v-list div:nth-child(2) .v-list__tile .v-list__tile__content .v-list__tile__title').click();
        await page.click('button:has-text("save Salveaza")');


        const toastr = await page.$("//div[@class='toast-message']");
        if (toastr) {
            expect(await toastr.textContent()).toContain("Category added");
        }
        await page.waitForTimeout(1000);


        // Lvl2 Category
        page.locator("(//i[contains(.,'add')])[last()]").click();
        await page.locator(".v-tabs__item").click();

        await page.fill("//input[contains(@aria-label,'Nume categorie *')]", cat2Name);
        await page.fill("//input[contains(@aria-label,'Heading categorie')]", cat2Name);
        await page.fill("//input[contains(@aria-label,'Cod *')]", "01");
        await page.fill("//input[contains(@aria-label,'Prioritate *')]", "1");
        await page.click(".flex:nth-child(1) .v-input--selection-controls__ripple");
        await page.click(".flex:nth-child(2) .v-input--selection-controls__ripple");
        await page.click(".ml-2:nth-child(3) .v-input--selection-controls__ripple");

        await page.click("//input[contains(@aria-label,'Tva *')]");
        await page.click("(//div[@class='v-list__tile__title'][contains(.,'19')])[1]");
        await page.click("//span[contains(.,'Salveaza')]");

        await page.waitForSelector("//div[@class='toast-message']");
        const toastr2 = await page.$("//div[@class='toast-message']");
        if (toastr2) {
            expect(await toastr2.textContent()).toContain("Category added");
        }
        await page.waitForTimeout(1000);

        // Lvl3 Category
        page.locator("(//i[contains(.,'add')])[last()]").click();
        await page.locator(".v-tabs__item").click();

        await page.fill("//input[contains(@aria-label,'Nume categorie *')]", cat3Name);
        await page.fill("//input[contains(@aria-label,'Heading categorie')]", cat3Name);
        await page.fill("//input[contains(@aria-label,'Cod *')]", "01");
        await page.fill("//input[contains(@aria-label,'Prioritate *')]", "1");
        await page.click(".flex:nth-child(1) .v-input--selection-controls__ripple");
        await page.click(".flex:nth-child(2) .v-input--selection-controls__ripple");
        await page.click(".ml-2:nth-child(3) .v-input--selection-controls__ripple");

        await page.click("//input[contains(@aria-label,'Tva *')]");
        await page.click("(//div[@class='v-list__tile__title'][contains(.,'19')])[1]");
        await page.click("//span[contains(.,'Salveaza')]");

        await page.waitForSelector("//div[@class='toast-message']");
        const toastr3 = await page.$("//div[@class='toast-message']");
        if (toastr3) {
            expect(await toastr3.textContent()).toContain("Category added");
        }
        await page.waitForTimeout(1000);

        // Lvl4 Category
        page.locator("(//i[contains(.,'add')])[last()]").click();
        await page.locator(".v-tabs__item").click();
        const newCat4 = cat4Name;
        await page.fill("//input[contains(@aria-label,'Nume categorie *')]", newCat4);
        await page.fill("//input[contains(@aria-label,'Heading categorie')]", newCat4);
        await page.fill("//input[contains(@aria-label,'Cod *')]", "01");
        await page.fill("//input[contains(@aria-label,'Prioritate *')]", "1");
        await page.click(".flex:nth-child(1) .v-input--selection-controls__ripple");
        await page.click(".flex:nth-child(2) .v-input--selection-controls__ripple");
        await page.click(".ml-2:nth-child(3) .v-input--selection-controls__ripple");

        await page.click("//input[contains(@aria-label,'Tva *')]");
        await page.click("(//div[@class='v-list__tile__title'][contains(.,'19')])[1]");
        await page.fill(".editr--content", "Kit-uri aplice\n" +
            "\n" +
            "Pe Doraly.ro, gasesti o gama variata de kit-uri pentru montarea aplicelor de diferite forme sau dimensiuni, care asteapta sa ajunga la tine.\n" +
            "\n" +
            "Alege-le pe cele de care ai nevoie, cumpara la bucata, set, bax, sau en-gros, si bucura-te de preturile mici si flexible in functie de cantitatea aleasa.\n" +
            "\n" +
            "Si stai fara griji, dupa confirmarea comenzii, o impachetam sigur si apoi o expediem prin curier, direct la usa ta, in cel mai scurt timp. ");

        // Add specs group
        await page.click(".v-form:nth-child(3) .v-btn");
        await page.fill("//div[@id='app']/div[4]/div/div/div[2]/div/div/div/div/div/div/div/input", "CARACTERISTICI GENERALE");
        await page.click(".green--text");
        await page.waitForTimeout(1000);

        // Add specs 1
        await page.click(".v-form:nth-child(1) .v-dialog__activator");
        await page.fill("//input[contains(@aria-label,'Nume proprietate*')]", "Masura");
        await page.click("//label[contains(.,'Filtru')]");
        await page.waitForSelector("//input[@aria-label='Tipul de filtru']");
        await page.click("//input[@aria-label='Tipul de filtru']");
        await page.click("//div[@class='v-list__tile__title'][contains(.,'CheckBox')]");
        await page.waitForSelector("//label[contains(.,'Varianta?')]");
        await page.click("//label[contains(.,'Varianta?')]");
        await page.click(".v-dialog--active .v-btn:nth-child(3)");
        // await page.waitForTimeout(2000);

        //  Add specs 2

        await page.click(".v-form:nth-child(1) .v-dialog__activator");
        await page.fill("//input[contains(@aria-label,'Nume proprietate*')]", "Culoare");
        await page.click("//label[contains(.,'Filtru')]");
        await page.waitForSelector("//input[@aria-label='Tipul de filtru']");
        await page.click("//input[@aria-label='Tipul de filtru']");
        await page.click("//div[@class='v-list__tile__title'][contains(.,'CheckBox')]");
        await page.click("//label[contains(.,'Varianta?')]");
        await page.click("//label[contains(.,'Afiseaza toate produsele cu variante?')]");
        await page.click("//label[contains(.,'Afiseaza poza produs?')]");
        await page.click(".v-dialog--active .v-btn:nth-child(3)");
        // await page.waitForTimeout(2000);

        // Add specs 3

        await page.click(".v-form:nth-child(1) .v-dialog__activator");
        await page.fill("//input[contains(@aria-label,'Nume proprietate*')]", "Material");
        await page.click("//label[contains(.,'Filtru')]");
        await page.click("//input[@aria-label='Tipul de filtru']");
        await page.waitForSelector("//div[@class='v-list__tile__title'][contains(.,'CheckBox')]");
        await page.click("//div[@class='v-list__tile__title'][contains(.,'CheckBox')]");
        await page.click(".v-dialog--active .v-btn:nth-child(3)");
        // await page.waitForTimeout(2000);

        await page.click("//span[contains(.,'Salveaza')]");
        await page.waitForSelector("//div[@class='toast-message']");
        const toastr4 = await page.$("//div[@class='toast-message']");
        if (toastr4) {
            expect(await toastr4.textContent()).toContain("Category added");
        }
        // await page.waitForTimeout(2000);


    });

    
});