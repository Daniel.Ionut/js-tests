import { test as base } from "@playwright/test";

const test = base.extend<{
   foo: string;
   bar: string;

}>({
    foo: "hello",
    bar: "bye"

});
export default test;
export const expect = test.expect;