import { PlaywrightTestConfig, devices } from "@playwright/test";

const config: PlaywrightTestConfig = {
    use:{
        headless: false,
    },
    projects:[
        {
        name: 'chromium',
        use: {...devices['Desktop Chrome']},
        },
    ],
    // globalSetup: require.resolve('./global-setup')
};
export default config;

